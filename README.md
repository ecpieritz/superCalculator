<h1 align = "center"> :fast_forward: Super Calculator :rewind: </h1>
<h3 align = "center"> pt-BR </h3>

## 🖥 Preview
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/119828931_1734297673391243_4121738869924066236_o.jpg?_nc_cat=106&_nc_sid=0debeb&_nc_eui2=AeHjxtXylCS9bDOBPxWRsyCgAtLRfp4bPoQC0tF-nhs-hBzebUwzxUiWHOBtoOwLRLLu8JtJ0kN8mbKorH9tLssI&_nc_ohc=VcvHsI1QSwoAX8YxRAK&_nc_ht=scontent.fbnu2-1.fna&oh=af10085fde3fdd598a7e970f88df1700&oe=5F8A4645" width = "700">
</p>

---

## 📖 About
<p>Build, using HTML, CSS and pure JavaScript, an application called Super Calculator.</p>

---

## 🛠 Technologies used
- CSS
- HTML
- Javascript

---

## :pushpin: Objectives
Exercise the following concepts:
- Use of HTML elements
- Use of CSS
- Use of identifiers in HTML elements
- Capture reference elements with JavaScript
- Event handling with JavaScript
- Implementation of functions with JavaScript

---

## 🚀 How to execute the project
#### Clone the repository
git clone https://github.com/EPieritz/superCalculator.git

#### Enter directory
`cd superCalculator`

#### Run the server
- right click on the `index.html` file
- click on `open with liveserver`

---
Developed with 💙 by Emilyn C. Pieritz
